package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	private static List<Department> union(List<Department> a, List<Department> b) {
		List<Department> c = new ArrayList<>();
		c.addAll(a);
		c.addAll(b);
		return c;
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		List<Department> temp = new ArrayList<>();
		temp.add(rootDepartment);
		temp = temp.stream().filter(i -> i != null).collect(Collectors.toList());
		Integer result = 0;
		while (temp.size() != 0) {
			++result;
			temp = temp.stream().map(i -> i.subDepartments).reduce((a, b) -> union(a, b)).orElse(new ArrayList<>())
					.stream().filter(i -> i != null).collect(Collectors.toList());
		}
		return result;
	}

}
