package com.binary_studio.uniq_in_sorted_stream;

//You CAN modify this class
public final class Row<RowData> {

	private final Long id;

	static long last;

	public static <T> Row<T> nullifyDuplicate(Row<T> row) {
		System.err.println(String.valueOf(last) + ", " + String.valueOf(row.getPrimaryId()));
		if (row.getPrimaryId() == last) {
			last = row.getPrimaryId();
			return null;
		}
		last = row.getPrimaryId();
		return row;
	}

	public Row(Long id) {
		this.id = id;
	}

	public Long getPrimaryId() {
		return this.id;
	}

}
