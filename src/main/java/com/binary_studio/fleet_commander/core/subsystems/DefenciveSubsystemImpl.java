package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String _name;

	private Integer _powergridConsumption;

	private Integer _capacitorConsumption;

	private Integer _impactReductionPercent;

	private Integer _shieldRegeneration;

	private Integer _hullRegeneration;

	private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this._name = name;
		this._powergridConsumption = powergridConsumption.value();
		this._capacitorConsumption = capacitorConsumption.value();
		this._impactReductionPercent = impactReductionPercent.value();
		this._shieldRegeneration = shieldRegeneration.value();
		this._hullRegeneration = hullRegeneration.value();
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		DefenciveSubsystemImpl result = new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption,
				impactReductionPercent, shieldRegeneration, hullRegeneration);
		return result;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return PositiveInteger.of(this._powergridConsumption);
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return PositiveInteger.of(this._capacitorConsumption);
	}

	@Override
	public String getName() {
		return this._name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		Double impactReduction = (100.0 - Math.min(this._impactReductionPercent.doubleValue(), 95.0)) / 100.0;
		Integer impact = incomingDamage.damage.value();
		Integer impactReduced = Integer.valueOf((int) Math.ceil(impact.doubleValue() * impactReduction));
		return new AttackAction(PositiveInteger.of(impactReduced), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(PositiveInteger.of(this._shieldRegeneration),
				PositiveInteger.of(this._hullRegeneration));
	}

}
