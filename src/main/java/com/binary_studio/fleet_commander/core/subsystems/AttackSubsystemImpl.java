package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String _name;

	private Integer _powergridRequirments;

	private Integer _capacitorConsumption;

	private Integer _optimalSpeed;

	private Integer _optimalSize;

	private Integer _baseDamage;

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage)
			throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this._name = name;
		this._powergridRequirments = powergridRequirments.value();
		this._capacitorConsumption = capacitorConsumption.value();
		this._optimalSpeed = optimalSpeed.value();
		this._optimalSize = optimalSize.value();
		this._baseDamage = baseDamage.value();
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		AttackSubsystemImpl result = new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption,
				optimalSpeed, optimalSize, baseDamage);
		return result;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return PositiveInteger.of(this._powergridRequirments);
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return PositiveInteger.of(this._capacitorConsumption);
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		Integer targetSize = target.getSize().value();
		Integer targetSpeed = target.getCurrentSpeed().value();
		Double sizeReductionModifier;
		Double speedReductionModifier;
		if (targetSize >= this._optimalSize) {
			sizeReductionModifier = 1.0;
		}
		else {
			sizeReductionModifier = targetSize.doubleValue() / this._optimalSize.doubleValue();
		}
		if (targetSpeed <= this._optimalSpeed) {
			speedReductionModifier = 1.0;
		}
		else {
			speedReductionModifier = this._optimalSpeed.doubleValue() / (2.0 * targetSpeed.doubleValue());
		}
		return PositiveInteger.of(Integer
				.valueOf((int) Math.ceil(this._baseDamage * Math.min(speedReductionModifier, sizeReductionModifier))));
	}

	@Override
	public String getName() {
		return this._name;
	}

}
