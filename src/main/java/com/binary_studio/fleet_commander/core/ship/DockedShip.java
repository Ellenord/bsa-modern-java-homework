package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String _name;

	private PositiveInteger _shieldHP;

	private PositiveInteger _hullHP;

	private PositiveInteger _powergridOutput;

	private PositiveInteger _capacitorAmount;

	private PositiveInteger _capacitorRechargeRate;

	private PositiveInteger _speed;

	private PositiveInteger _size;

	private AttackSubsystem _attack;

	private DefenciveSubsystem _defencive;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, AttackSubsystem attack, DefenciveSubsystem defencive) {
		this._name = name;
		this._shieldHP = shieldHP;
		this._hullHP = hullHP;
		this._powergridOutput = powergridOutput;
		this._capacitorAmount = capacitorAmount;
		this._capacitorRechargeRate = capacitorRechargeRate;
		this._speed = speed;
		this._size = size;
		this._attack = attack;
		this._defencive = defencive;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size, null, null);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this._attack = null;
			return;
		}
		Integer powerGridSum = subsystem.getPowerGridConsumption().value();
		if (this._defencive != null) {
			powerGridSum += this._defencive.getPowerGridConsumption().value();
		}
		if (powerGridSum > this._powergridOutput.value()) {
			throw new InsufficientPowergridException(powerGridSum - this._powergridOutput.value());
		}
		this._attack = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this._defencive = null;
			return;
		}
		Integer powerGridSum = subsystem.getPowerGridConsumption().value();
		if (this._attack != null) {
			powerGridSum += this._attack.getPowerGridConsumption().value();
		}
		if (powerGridSum > this._powergridOutput.value()) {
			throw new InsufficientPowergridException(powerGridSum - this._powergridOutput.value());
		}
		this._defencive = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this._attack == null && this._defencive == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this._attack == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this._attack == null || this._defencive == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this._name, this._shieldHP, this._hullHP, this._powergridOutput,
				this._capacitorAmount, this._capacitorRechargeRate, this._speed, this._size, this._attack,
				this._defencive);
	}

}
