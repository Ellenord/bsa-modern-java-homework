package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String _name;

	private Integer _shieldHP;

	private Integer _currentShieldHP;

	private Integer _hullHP;

	private Integer _currentHullHP;

	private Integer _powergridOutput;

	private Integer _capacitorAmount;

	private Integer _currentCapacitorAmount;

	private Integer _capacitorRechargeRate;

	private Integer _speed;

	private Integer _size;

	private AttackSubsystem _attack;

	private DefenciveSubsystem _defencive;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attack, DefenciveSubsystem defencive) {
		this._name = name;
		this._shieldHP = shieldHP.value();
		this._currentShieldHP = shieldHP.value();
		this._hullHP = hullHP.value();
		this._currentHullHP = hullHP.value();
		this._powergridOutput = powergridOutput.value();
		this._capacitorAmount = capacitorAmount.value();
		this._currentCapacitorAmount = capacitorAmount.value();
		this._capacitorRechargeRate = capacitorRechargeRate.value();
		this._speed = speed.value();
		this._size = size.value();
		this._attack = attack;
		this._defencive = defencive;
	}

	@Override
	public void endTurn() {
		this._currentCapacitorAmount = Math.min(this._capacitorAmount,
				this._currentCapacitorAmount + this._capacitorRechargeRate);
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this._name;
	}

	@Override
	public PositiveInteger getSize() {
		return PositiveInteger.of(this._size);
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return PositiveInteger.of(this._speed);
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Integer capacitorConsumption = this._attack.getCapacitorConsumption().value();
		if (capacitorConsumption > this._currentCapacitorAmount) {
			return Optional.empty();
		}
		AttackAction attackAction = new AttackAction(this._attack.attack(target), this, target, this._attack);
		this._currentCapacitorAmount -= capacitorConsumption;
		return Optional.of(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		attack = this._defencive.reduceDamage(attack);
		Integer damage = attack.damage.value();
		if (damage >= this._currentShieldHP + this._currentHullHP) {
			return new AttackResult.Destroyed();
		}
		if (this._currentShieldHP < damage) {
			this._currentShieldHP = 0;
			damage -= this._currentShieldHP;
			this._currentHullHP -= damage;
		}
		else {
			this._currentShieldHP -= damage;
		}
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Integer capacitorConsumption = this._defencive.getCapacitorConsumption().value();
		if (capacitorConsumption > this._currentCapacitorAmount) {
			return Optional.empty();
		}
		this._currentCapacitorAmount -= capacitorConsumption;
		RegenerateAction regenerateAction = this._defencive.regenerate();
		Integer shieldRegenerated = Math.min(this._shieldHP,
				this._currentShieldHP + regenerateAction.shieldHPRegenerated.value()) - this._currentShieldHP;
		Integer hullRegenerated = Math.min(this._hullHP,
				this._currentHullHP + regenerateAction.hullHPRegenerated.value()) - this._currentHullHP;
		this._currentShieldHP += shieldRegenerated;
		this._currentHullHP += hullRegenerated;
		return Optional
				.of(new RegenerateAction(PositiveInteger.of(shieldRegenerated), PositiveInteger.of(hullRegenerated)));
	}

}
